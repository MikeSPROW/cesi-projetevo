#!/bin/sh

echo "Archive and clean SMB share \Commun"
cd mnt/volume
tar -cvzf "archive_commun_$(date +%F).tar" /mnt/commun
rm -rf /mnt/commun/*

echo "Backup Windows AD config"
cd /mnt/volume
tar -cvzf "archive_ad-config_$(date +%F).tar" /mnt/nfs

echo "Clean archive older than 21 days"
find /mnt/volume/* -mtime +21 -exec rm {} \;
