#!/bin/bash

echo "docker-smb01.cyc.local" > /etc/hostname

echo $AdminPass | adcli join $DomainName -U $AdminName

echo "session optional        pam_mkhomedir.so skel=/etc/skel umask=077" > /etc/pam.d/common-session

mkdir /prefix.win/
samba-tool domain join cyc.local RODC -U $AdminName --password=$AdminPass --targetdir=/prefix.win/
