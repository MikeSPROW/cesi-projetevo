#!/bin/sh

# Setup resolv.conf
echo "nameserver $dns1
nameserver $dns2" >> /etc/resolv.conf

# Setup /etc/bind permitions
chown -R named:root /etc/bind
chmod 770 /etc/bind
chmod 770 /etc/bind/*

# Setup /var/bind permitions
mkdir /var/bind
chown -R named:root /var/bind
chmod 770 /var/bind
chmod 770 /var/bind/*

# Start bind9
named -c /etc/bind/named.conf -g -u root