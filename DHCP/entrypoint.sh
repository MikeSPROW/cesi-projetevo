#!/bin/sh

chown dhcpd:dhcpd /etc/dhcp/dhcpd.conf
chmod 770 /etc/dhcp/dhcpd.conf

mkdir -p /var/lib/dhcp/
chown dhcpd:dhcpd /var/lib/dhcp/
chmod 770 /var/lib/dhcp/

touch /var/lib/dhcp/dhcpd.leases

/usr/sbin/dhcpd -4 -f -d --no-pid -cf /etc/dhcp/dhcpd.conf